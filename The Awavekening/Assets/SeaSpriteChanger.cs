﻿using System.Collections;
using UnityEngine;

public class SeaSpriteChanger : MonoBehaviour {
    public SpriteRenderer sRend;
    public Sprite[] sea;

    void Start() {
        sRend = GetComponent<SpriteRenderer>();
        StartCoroutine("PlayText");
    }

    IEnumerator PlayText() {
        for (int i = 0; i < sea.Length; i++) {
            sRend.sprite = sea[i];
            yield return new WaitForSeconds(1f);
            if (i == sea.Length - 1) {
                i = 0;
            }
        }
    }

}
