﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class DaveDir : MonoBehaviour {


    bool dying = false;

    Rigidbody2D parentRb;

    SpriteRenderer sRend;
    public Sprite[] daveSprites = new Sprite[8];
    public Sprite[] daveSpritesCostume = new Sprite[8];

    float half45 = 22.5f;
    Transform parentTrans;

    public int waveAngleThreshold;
    float speed = 10f;

    public AudioClip[] eatingAudio = new AudioClip[4];
    public AudioClip waveHit;
    AudioSource audio;

    PlayerStats pStats;
    Vector3 originalScale;

    public Animator anim;

    public GameObject gameOverPanel;
    public Text scoreText;
    public Text currencyText;

    public Sprite[] deathAnim;


    void Start() {

        parentRb = transform.parent.GetComponent<Rigidbody2D>();
        pStats = PlayerStats.instance;
        sRend = GetComponent<SpriteRenderer>();
        parentTrans = transform.parent.GetComponent<Transform>();
        audio = GetComponent<AudioSource>();
        speed = speed + pStats._startSpeed;


        if (pStats._gotHat) {
            daveSprites = daveSpritesCostume;
        }
    }

    void FixedUpdate() {


        parentRb.AddForce(-parentTrans.right * speed);

        if (Input.GetKey(KeyCode.A)) {
            parentRb.rotation += 1 + pStats._handeling;
        }
        if (Input.GetKey(KeyCode.D)) {
            parentRb.rotation -= 1 + pStats._handeling;
        }
        if (speed <= 15 && !pStats._startScene) {
            GameOver();
        }


        Vector3 rot = -parentTrans.eulerAngles;

        transform.rotation = Quaternion.Euler(Vector3.up);

        int pAngle = (int)parentTrans.eulerAngles.z;
        if (!dying) {


            if (pAngle > 270 - half45 && pAngle < 270 + half45) {
                sRend.sprite = daveSprites[0];
            } else if (pAngle > 225 + -half45 && pAngle < 225 + half45) {
                sRend.sprite = daveSprites[1];
            } else if (pAngle > 180 + -half45 && pAngle < 180 + half45) {
                sRend.sprite = daveSprites[2];
            } else if (pAngle > 135 + -half45 && pAngle < 135 + half45) {
                sRend.sprite = daveSprites[3];
            } else if (pAngle > 90 - half45 && pAngle < 90 + half45) {
                sRend.sprite = daveSprites[4];
            } else if (pAngle > 45 - half45 && pAngle < 45 + half45) {
                sRend.sprite = daveSprites[5];
            } else if (pAngle > -half45 && pAngle < half45) {
                sRend.sprite = daveSprites[6];
            } else if (pAngle > 315 - half45 && pAngle < 315 + half45) {
                sRend.sprite = daveSprites[7];
            }
        }
    }

    void GameOver() {
        if (!dying) {
            dying = true;
            parentRb.velocity = Vector3.zero;
            speed = 0;
            StartCoroutine("DeathAnimation");
        }
    }

    IEnumerator DeathAnimation() {

        for (int i = 0; i < deathAnim.Length; i++) {
            sRend.sprite = deathAnim[i];
            yield return new WaitForSeconds(0.15f);
        }

        yield return new WaitForSeconds(0.25f);

        gameOverPanel.SetActive(true);

        scoreText.text = "Score: " + pStats._score; //Here should be + the final score
        currencyText.text = "Shells: " + pStats._gold;

        gameObject.SetActive(false);
        yield return new WaitForSeconds(0.25f);


    }

    void OnTriggerExit2D(Collider2D _col) {
        if (_col.gameObject.CompareTag("End")) {
            GameOver();
        }
    }

    void OnTriggerEnter2D(Collider2D _col) {
        if (_col.gameObject.CompareTag("Wave")) {
            int angleDifference = ((int)Mathf.Abs(_col.transform.parent.transform.eulerAngles.z)) - (int)Mathf.Abs(transform.parent.transform.eulerAngles.z);
            if (Mathf.Abs(angleDifference) < waveAngleThreshold) {
                eating();
                speed += 5;
                transform.localScale = transform.localScale * 1.2f;
                pStats._score += 100;
            } else {
                audio.clip = waveHit;
                audio.Play();
                speed -= 5;

            }
            Destroy(_col.gameObject);
        }

        if (_col.gameObject.CompareTag("Coin")) {
            eating();
            pStats._gold += pStats._coinGoldValue;
            Destroy(_col.gameObject);
        }
    }

    [ContextMenu("testwave")]
    public void TestWave() {
        GameOver();
    }

    void eating() {
        if (audio.isPlaying) return;
        audio.clip = eatingAudio[Random.Range(0, eatingAudio.Length)];
        audio.Play();
    }


}
