﻿using UnityEngine;

public class WaveDir : MonoBehaviour {

    Transform parentTrans;

    public Animator anim;

    SpriteRenderer sRend;

    void Start() {
        parentTrans = GetComponentInParent<Transform>();
        sRend = GetComponent<SpriteRenderer>();
        Vector3 rot = transform.eulerAngles - parentTrans.eulerAngles;
        transform.rotation = Quaternion.Euler(rot);

    }

    void FixedUpdate() {
        if (transform.parent.GetComponent<Rigidbody2D>().velocity.x > 0) {
            sRend.flipX = true;
        }
    }

}


