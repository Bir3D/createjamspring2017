﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreAndSchells : MonoBehaviour {

    PlayerStats pStats;
    public Text score;
    public Text shells;

    void Start() {
        pStats = PlayerStats.instance;
    }

    // Update is called once per frame
    void Update() {
        score.text = pStats._score.ToString();
        shells.text = pStats._gold.ToString();
    }
}
