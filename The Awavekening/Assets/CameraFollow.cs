﻿using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public float cameraFollowSpeed = 2f;

    Transform playerPos;

    void Start() {
        playerPos = GameObject.FindGameObjectWithTag("Player").transform;

    }

    void FixedUpdate() {
        if (playerPos != null) {
            Vector3 targetPos = new Vector3(playerPos.transform.position.x, playerPos.transform.position.y, transform.position.z);
            transform.position = Vector3.Lerp(transform.position, targetPos, Time.deltaTime * cameraFollowSpeed);
        }
    }
}
