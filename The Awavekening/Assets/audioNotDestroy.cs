﻿using UnityEngine;

public class audioNotDestroy : MonoBehaviour {

    void Awake() {
        DontDestroyOnLoad(transform.gameObject);
    }
}
