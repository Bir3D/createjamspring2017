﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class shopScript : MonoBehaviour {

    public Button startText;
    public Button buyText;
    public Button menuText;
    public AudioSource sound;
    PlayerStats pStats;
    bool msgBool = false;
    string msg;

    // Use this for initialization
    void Start() {
        startText = startText.GetComponent<Button>();
        menuText = menuText.GetComponent<Button>();
        buyText = buyText.GetComponent<Button>();
        pStats = PlayerStats.instance;
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            sound.Play();
            pStats._gold = 500;
        }
    }

    public void buyItem() {
        sound.Play();
        if (pStats._gold >= 500) {
            msg = "Purchase complete! Go play now!";
            pStats._gold -= 500;
            msgBool = true;
            pStats._gotHat = true;
        } else {
            msg = "Not enough shells! Go collect some more!";
            msgBool = true;
        }
    }

    void OnGUI() {
        if (msgBool) {
            GUI.Box(new Rect(Screen.width / 2 - 450 / 2, Screen.height / 1.5f, 450, 25), msg);
            Invoke("finalOver", 4f);
        }

    }
    void finalOver() {
        msgBool = false;
    }
    public void goToMenu() {
        sound.Play();
        SceneManager.LoadScene(3);
    }

    public void StartLevel() //this function will be used on our Play button

    {
        sound.Play();
        SceneManager.LoadScene(1); //this will load our first level from our build settings. "1" is the second scene in our game

    }
}
