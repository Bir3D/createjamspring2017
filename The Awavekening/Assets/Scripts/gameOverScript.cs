﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class gameOverScript : MonoBehaviour {

    public Button shopText;
    public Button startText;

    // Use this for initialization
    void Start() {
        shopText = shopText.GetComponent<Button>();
        startText = startText.GetComponent<Button>();
    }

    // Update is called once per frame
    void Update() {

    }

    public void StartLevel() //this function will be used on our Play button
    {
        Debug.Log("I work");
        SceneManager.LoadScene(1); //this will load our first level from our build settings. "1" is the second scene in our game

    }

    public void goToShop() {
        SceneManager.LoadScene(2);
    }
}
