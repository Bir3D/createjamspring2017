﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// attach to UI Text component (with the full text already there)

public class DialogText : MonoBehaviour {
    AudioSource audio;
    public AudioClip[] keyAudio = new AudioClip[5];

    PlayerStats pStats;

    Text txt;
    string story, story2, story3;

    void Awake() {
        pStats = PlayerStats.instance;
        audio = GetComponent<AudioSource>();
        txt = GetComponent<Text>();
        story = "Hello, me name is Dave. And I'm a wave.";
        story2 = "I need to be the biggest wave. So I eat my friends.";
        story3 = "Help me collect seashells and beat my friends with A and D";
        txt.text = "";

        StartCoroutine("PlayText");
    }


    IEnumerator PlayText() {
        foreach (char c in story) {
            type();
            txt.text += c;
            yield return new WaitForSeconds(0.2f);
        }
        yield return new WaitForSeconds(2f);
        txt.text = "";

        foreach (char c in story2) {
            type();
            txt.text += c;
            yield return new WaitForSeconds(0.2f);
        }
        yield return new WaitForSeconds(2f);
        txt.text = "";
        foreach (char c in story3) {
            type();
            txt.text += c;
            yield return new WaitForSeconds(0.2f);
        }
        yield return new WaitForSeconds(5f);
        pStats._gold = 0;
        SceneManager.LoadScene(3);
    }
    void type() {
        audio.clip = keyAudio[Random.Range(0, keyAudio.Length)];
        audio.Play();
    }

    public Image fadeOut;
    public bool canFade;
    private Color alphaColor;
    private float timeToFade = 1.0f;

    public void Start() {
        canFade = false;
    }
    public void FixedUpdate() {
        if (canFade) {
            float alpha = Mathf.Lerp(0, 255, Time.time);
            fadeOut.color = new Color(0, 0, 0, alpha);
        }
    }

}