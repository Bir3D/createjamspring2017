﻿using UnityEngine;

public class WaveSpawner : MonoBehaviour {

    public GameObject[] wavePrefab;
    public Transform[] waveSpawnPoints;
    public Transform player;
    public Transform midPoint;
    public int spawnAngle = 45;
    public float waveSpawnDelay = 2f;

    void Start() {
        InvokeRepeating("WaveSpawn", 2f, waveSpawnDelay);
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update() {

    }

    void WaveSpawn() {
        int rWave = Random.Range(0, wavePrefab.Length);
        int rSpawn = Random.Range(0, waveSpawnPoints.Length);
        int rAngle = Random.Range(-spawnAngle, spawnAngle);
        GameObject tempWave = Instantiate(wavePrefab[rWave], waveSpawnPoints[rSpawn].position, Quaternion.Euler(0, 0, rSpawn * 45 + rAngle));
    }

}
