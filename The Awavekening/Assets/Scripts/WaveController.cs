﻿using UnityEngine;

public class WaveController : MonoBehaviour {

    float speed = 0.7f;

    public Rigidbody2D rb;

    void Start() {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update() {
        rb.AddForce(-transform.right * speed);

    }
}
