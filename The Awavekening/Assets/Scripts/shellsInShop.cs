﻿using UnityEngine;
using UnityEngine.UI;

public class shellsInShop : MonoBehaviour {

    public Text currencyText;

    PlayerStats pStats;

    // Use this for initialization
    void Start() {
        pStats = PlayerStats.instance;
        Debug.Log(pStats._gold.ToString());
        currencyText.text = pStats._gold.ToString();
    }

}
