﻿using UnityEngine;

public class CoinSpawner : MonoBehaviour {

    public GameObject coinPrefab;

    Transform spawnArea;
    int widthA;
    int heightA;

    PlayerStats pStats;


    void Start() {
        pStats = PlayerStats.instance;
        InvokeRepeating("CoinSpawnerFunc", 0f, pStats._coinSpawnRate);
        spawnArea = GetComponent<Transform>();

        widthA = (int)spawnArea.localScale.x;
        heightA = (int)spawnArea.localScale.y;



    }

    void Update() {

    }

    public void CoinSpawnerFunc() {
        int rx = Random.Range(0, widthA);
        int ry = Random.Range(0, heightA);

        Instantiate(coinPrefab, new Vector2(rx - widthA / 2, ry - heightA / 2), Quaternion.identity);

    }
}
