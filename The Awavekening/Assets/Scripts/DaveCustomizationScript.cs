﻿using UnityEngine;
using UnityEngine.UI;

public class DaveCustomizationScript : MonoBehaviour {

    public Button[] buttonArr;
    public Sprite[] sprArr;
    public Image daveSpr;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void onButton(int num) {
        daveSpr.sprite = sprArr[num];
    }
}
