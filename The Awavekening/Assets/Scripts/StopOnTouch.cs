﻿using UnityEngine;

public class StopOnTouch : MonoBehaviour {


    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.gameObject.CompareTag("Wave")) {
            Destroy(collision.transform.parent.gameObject);
        }

    }
}
