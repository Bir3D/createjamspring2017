﻿using UnityEngine;

public class PlayerStats : MonoBehaviour {


    public static PlayerStats instance;

    public bool _startScene = true;

    //Upgrades
    public float _startSpeed;
    public float _windResistnece;
    public float _handeling = 1;
    public float _coinSpawnRate;
    public int _coinGoldValue = 10;
    public bool _gotHat = false;

    //Player stats
    public int _gold;
    public int _score;


    void Awake() {
        if (instance != null) {
            Debug.LogError("More than one BuildManager in scene!");
            return;
        }
        instance = this;

    }
}

