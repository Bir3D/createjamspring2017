﻿using UnityEngine;

public class SceneLoadingScript : MonoBehaviour {

    PlayerStats pStats;
    void Awake() {
        pStats = PlayerStats.instance;

        pStats._startScene = false;

        pStats._score = 0;
        pStats._startSpeed = 30;
        if (pStats._gotHat) {
            pStats._startSpeed *= 2;

        }


    }

}
